import java.util.Random;
import java.util.Scanner;
/**
 * 
 * @author Micha�
 *
 */
public class GameApplication {
	
	private	static Scanner input = new Scanner(System.in); 	//eclipse force me to make static
	private static Random generator = new Random();			//eclipse force me to make static
	
	private static int size = 20;
	private static int InitialNumberOfbonks = 8;
	private static int InitialNumberOfzaps = 5;
	private static GridWorld gridWorld =  new GridWorld(size);

	
	public static void main(String[] args) throws InterruptedException{
		
		String userDecision;
				
		do{
			
			//System.out.println("Give me GridWorld Size (int):");			// input.nextInt();
			for(int i=1; i <= InitialNumberOfbonks; i++){
				
				int row = generator.nextInt(size);			//generates int from 0 to size.
				int col = generator.nextInt(size);			
				
				Position pos = new Position(row, col);
				Room room = gridWorld.getRoom(pos);			//wez pokoj o tej pozycji
				Bonk bonk = new Bonk();
				bonk.setName("b"+i);
				room.addBonk(bonk);							//i dodaj do niego bonka	
				gridWorld.printGrid();
				//I need to remove bonk from current room if i want to move them
				Thread.sleep(500);	//500ms
				System.out.println("******************************************");	
			}
			
			System.out.println("***And Now Zaps***");
			
			for(int i=1; i <= InitialNumberOfzaps; i++){
				
				int row = generator.nextInt(size);
				int col = generator.nextInt(size);	
				
				Position pos = new Position(row, col);
				Room room = gridWorld.getRoom(pos);			
				Zap zap = new Zap();
				zap.setName("z"+i);
				room.addZap(zap);		
				gridWorld.printGrid();
				//I need to remove Zap from current room if i want to move them
				Thread.sleep(500);
				System.out.println("******************************************");	
			}
			
			System.out.println("the end of the initial filing of Bonks and Zaps\n");
			System.out.println("List of the Bonks and Zaps and its positions:");
			gridWorld.printListOfBonksAndZaps();
			
			//Why not call method .printListOfBonks() in this place
				
			System.out.println("Do you want to continue ?(yes/no)");
			userDecision = input.next();
					
		}while(userDecision.equals("yes"));
		
			System.out.println("*****ByBy*****");
	}
}
