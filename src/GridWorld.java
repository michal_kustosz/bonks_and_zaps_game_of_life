/**
 * 
 * @author Micha�
 *
 */

public class GridWorld {

	private Room[][] array2D;
	private Position roomposition;
	
	/**
	 * 
	 * @param size
	 */

	public GridWorld(int size) {

		array2D = new Room[size][size];

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				roomposition = new Position(i, j);
				array2D[i][j] = new Room(roomposition);
			}
		}
	} //////////////////////////////////////// end of constructor

	public void printListOfBonksAndZaps() {			//something is wrong b.getLocation() return null
		for (int x = 0; x < 20; x++) {
			for (int y = 0; y < 20; y++) {
				Position pos = new Position(x, y);
				if ( !getRoom(pos).getBonks().isEmpty() ) {
					for ( Bonk b : getRoom(pos).getBonks() ) {
						System.out.println(b.getName() + " " + b.getLocation().getRow() + "-" + b.getLocation().getCol());
					}
				}
			}
		}
	}//////////////////////////////////////////////
	public void printGrid() {
		for (int i = 0; i < array2D.length; i++) {
			for (int j = 0; j < array2D.length; j++) {
				// System.out.print(array2D[i][j].getPosition().toString());
				System.out.print(array2D[i][j].toString());
			}
			System.out.println();
		}
	}
	/////////////////////////////////////////////////
	public Room getRoom(Position p) {
		return array2D[p.row][p.col];
	}
	/////////////////////////////////////////////////
}
