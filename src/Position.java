/**
 * 
 * @author Micha�
 *
 */
public class Position {
	
	public int row;
	public int col;
	
	//constructor
	public Position(int x, int y){
		row = x;
		col = y;
	}
	
	public String toString(){
		return " [ row = "+row+" ; col = "+col+" ] ";
	}
	
	public int getCol(){
		return col;
	}

	public int getRow(){
		return row;
	}

}
