/**
 * 
 * @author Micha�
 *
 */
public interface Being {

	public String getName();
	
	public void act(); //throws CannotActException;
	
	public Position getLocation();
	
	public void setLocation();
	
}
