import java.util.ArrayList;
/**
 * 
 * @author Micha�
 *
 */
public class Room {

	private Position pos;
	private ArrayList <Bonk> bonks; 
	private ArrayList <Zap> zaps;
	
	/**
	 * 
	 * @param p
	 */
	
	public Room(Position p){
		pos = p;
		bonks = new ArrayList <Bonk>();
		zaps = new ArrayList <Zap>();
	}
	
	public ArrayList<Bonk> getBonks(){
		return bonks;
	}
	
	public Position getPosition(){
		return pos;						//return position row, col of the particular room in 2D array
	}
	//////////////////////////////////
	public void addBonk(Bonk b){
		bonks.add(b);
	}
	//////////////////////////////////
	public void addZap(Zap z){
		zaps.add(z);
	}
	/////////////////////////////////	
	public String toString(){
		
		String str = " [ ";
		
		if( bonks.isEmpty() && zaps.isEmpty() ){
			str += " ";
		}
		else if(!bonks.isEmpty()){				//if its NOT empty
			str+="b";
		}
		else if(!zaps.isEmpty()){
			str+="z";
		}
		str+=" ] ";
			
		return str;
	}
	////////////////////////////////////////////////
	
}
